<%@page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page isELIgnored="false" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<link href="./css/style.css" rel="stylesheet" type="text/css">
		<title>簡易Twitter</title>
	</head>
	<body>
		<div class="main-contents">
			<!-- リンク集 -->
			<c:if test="${ empty loginUser }">
				<a href="login">ログイン</a>
				<a href="signup">登録する</a>
			</c:if>

			<!-- ログイン情報 -->
			<c:if test="${not empty loginUser }">
				<a href="./">ホーム</a>
				<a href="settings">設定</a>
				<a href="logout">ログアウト</a>

				<!-- ログイン情報 -->
				<div class="profile">
					<!-- ユーザー名の表示 -->
					<div class="name">
						<h2><c:out value="${ loginUser.name }"/></h2>
					</div>
					<!-- アカウント名 -->
					<div class="account">
						@<c:out value="${ loginUser.account }" />
					</div>
					<!-- 説明 -->
					<div class="discription">
						<c:out value="${ loginUser.description }" />
					</div>
				</div>
			</c:if>

			<!-- 投稿フォーム -->
			<div class="form-area">
				<!-- isShowMessageFormは変数 -->
				<c:if test="${ isShowMessageForm }">
					<form action="newMessage" method="post">
						いま、どうしてる？ <br />
						<textarea name="message" cols="100" rows="5" class="tweet-box"></textarea>
						<br />
						<input type="submit" value="つぶやく">(140文字まで)
					</form>
				</c:if>
			</div>

			<div class="messages">
				<c:forEach items="${ messages }" var="message">
					<div class="message">
						<div class="account-name">
							<!-- 投稿者のアカウントを表示 -->
							<span class="account">
								<c:out value="${ message.account }" />
							</span>

							<!-- 投稿者の名前を表示 -->
							<span class="name">
							 <c:out value="${ message.name }" />
							</span>
						</div>

						<!-- 投稿内容を表示 -->
						<div class="text">
							<c:out value="${ message.text }"/>
						</div>
						<div class="date">
							<c:out value="${ message.created_date }" />
						</div>
					</div>
				</c:forEach>
			</div>

		</div>

		<div class="copyright"> Copyright(c)yourName </div>
	</body>
</html>