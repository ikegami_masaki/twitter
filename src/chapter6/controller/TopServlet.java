package chapter6.controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import chapter6.beans.User;
import chapter6.beans.UserMessage;
import chapter6.service.MessageService;

/**
 * Servlet implementation class TopServlet
 */
@WebServlet(urlPatterns = { "/index.jsp" })
public class TopServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

//		セッションを取得して、その中からログイン情報を取得する
//		Userオブジェクトにキャストする
		User user = (User) request.getSession().getAttribute("loginUser");
		boolean isShowMessageForm;

//		ログイン中か
		if(user != null) {
			isShowMessageForm = true;
		} else {
			isShowMessageForm = false;
		}

//		データベースから投稿を取得
		List<UserMessage> messages = new MessageService().getMessage();

//		投稿フォームを表示するかをリクエストで返す
		request.setAttribute("messages", messages);
		request.setAttribute("isShowMessageForm", isShowMessageForm);

		request.getRequestDispatcher("/top.jsp").forward(request, response);
	}
}
