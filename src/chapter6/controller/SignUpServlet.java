package chapter6.controller;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import chapter6.beans.User;
import chapter6.service.UserServise;

/**
 * Servlet implementation class SignUpServlet
 */
@WebServlet(urlPatterns = { "/signup" })
public class SignUpServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		request.getRequestDispatcher("signup.jsp").forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

//		表示するメッセージのlist
		List<String> messages = new ArrayList<String>();

//		ユーザーが持っているセッション情報を取得
		HttpSession session = request.getSession();
		System.out.println(request.getParameterMap());

//		ユーザーを作成
		User signupUser = getUser(request);

		if(isValid(request, messages) == true){

			new UserServise().register(signupUser);
			response.sendRedirect("./");
		} else {
			request.setAttribute("signupUser", signupUser);
			session.setAttribute("errorMessages", messages);
			response.sendRedirect("signup");
		}
	}

//	パラメーター情報の設定
	private User getUser(HttpServletRequest request) {
		User user = new User();

		user.setName(request.getParameter("name"));
		user.setAccount(request.getParameter("account"));
		user.setPassword(request.getParameter("password"));
		user.setEmail(request.getParameter("email"));
		user.setDescription(request.getParameter("description"));

		return user;

	}

//	フォームのパラメーターの内容確認
	private boolean isValid(HttpServletRequest request, List<String> messages){
//		フォームの中から情報を取得
		String account = request.getParameter("account");
		String password = request.getParameter("password");

//		nullでも例外処理されないようにこのクラスを使用
//		入力内容が入っているかを確認
		if(StringUtils.isEmpty(account) == true) {
			messages.add("アカウント名を入力してください");
		}
		if (StringUtils.isEmpty(password)) {
			messages.add("パスワードを入力してください");
		}

		//TODO アカウントがすでに利用されていないか、メールアドレスがすでに登録されていないかを確認する処理を追加
//		エラーメッセージがないか確認
		if (messages.size() == 0) {
			return true;
		} else {
			return false;
		}
	}

}
